export enum ROL {
  ADMIN = 1,
  NORMAL = 0,
}

export const rolMap: { [T in ROL]: string } = {
  [ROL.ADMIN]: 'Administrator',
  [ROL.NORMAL]: 'Normal',
};
