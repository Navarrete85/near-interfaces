export enum STATE {
  FREE = 0,
  BUSY = 1,
}

export const stateMap: { [T in STATE]: string } = {
  [STATE.BUSY]: 'Occupied',
  [STATE.FREE]: 'Free',
};
