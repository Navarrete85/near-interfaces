export enum GENDER {
  MALE = 0,
  FEMALE = 1,
}

export const genderMap: { [P in GENDER]: string } = {
  [GENDER.MALE]: 'Masculino',
  [GENDER.FEMALE]: 'Femenino',
};
