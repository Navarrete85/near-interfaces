import { GENDER, genderMap } from './enums/gender-enum';
import { ROL, rolMap } from './enums/rol-enums';
import { STATE, stateMap } from './enums/state-enums';

const ENUMS = {
  GENDER,
  ROL,
  STATE,
};

const ENUMS_MAP = {
  genderMap,
  rolMap,
  stateMap,
};

export { ENUMS, ENUMS_MAP };
