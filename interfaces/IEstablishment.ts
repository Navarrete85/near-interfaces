export interface IEstablishment {
  _id: string;
  occupation: number;
  offers: Array<string>;
  description: string;
  image_logo: string;
  location: ILocation;
  name: string;
  photo_url: Array<string>;
  uuid: string;
}

export interface ILocation {
  lat: string;
  long: string;
}
