export interface IOfer {
  _id: string;
  name: string;
  description: string;
  establishment_id: string;
}
