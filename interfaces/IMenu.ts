export interface IMenu {
  _id: string;
  establishment_id: string;
  urlPdf: IUrlPdf;
  cart: Array<ICartElement>;
}

export interface IUrlPdf {
  _id: string;
  url: string;
}

export interface ICartElement {
  _id: string;
  section: ICartSection;
}

export interface ICartSection {
  name: string;
  products: Array<IProductCard>;
}

export interface IProductCard {
  _id: string;
  name: string;
  description: string;
  price: number;
  allergens: Array<string>;
}
