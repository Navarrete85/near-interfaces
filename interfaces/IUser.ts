import { GENDER } from '../enums/gender-enum';
import { ROL } from '../enums/rol-enums';

export interface IUser {
  _id: string;
  connect: boolean;
  favorites: Array<string>;
  name: string;
  email: string;
  lastName: string;
  password: string;
  gender: GENDER;
  photo_url: string;
  active: boolean;
  rol: ROL;
  address: string;
  phone_number: number;
}
