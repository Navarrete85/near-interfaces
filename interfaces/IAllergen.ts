export interface IAllergen {
  _id: string;
  name: string;
  image: IImage;
}

export interface IImage {
  _id: string;
  url: string;
}
