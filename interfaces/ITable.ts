export interface ITables {
  _id: string;
  establishment_id: string;
  sections: Array<IEstablishmentSection>;
}

export interface IEstablishmentSection {
  _id: string;
  name: string;
  tables: Array<ITable>;
}

export interface ITable {
  _id: string;
  uuid: string;
  identifier: string;
  order: number;
  state: string;
  start_time?: string;
  time_of_state_change?: string;
}
